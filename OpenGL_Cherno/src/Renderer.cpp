#include "Renderer.h"

void Renderer::Clear() const
{
	GlCall(glClear(GL_COLOR_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray* va, const IndexBuffer* ib, const Shader* shader) const
{
	va->Bind();
	ib->Bind();
	shader->Bind();

	GlCall(glDrawElements(GL_TRIANGLES, ib->GetCount(), GL_UNSIGNED_INT, nullptr));
}